// ConsoleApplication7.cpp: определяет точку входа для консольного приложения.
//перевод секунд в дни, часы, минуты.
#include "stdafx.h"
#include "iostream"
#include "conio.h"

using std::cout;
using std::endl;
using std::cin;



int main()
{
	setlocale(LC_ALL, "Russian");
	int sek;//количество секунд
	int min;//количество минут
	int hours;//количество часов
	int days;//количество дней
	
	cout << "введите количество секунд:";
	cin >> sek;

	min = sek / 60;
	hours = min / 60;
	days = hours / 24;


	cout << "количество дней:" << days << endl;
	cout << "количество часов:" << hours << endl;
	cout << "количество минут:" << min << endl;

	_getch();


    return 0;
}

